#!/usr/bin/env python3

import argparse
import ipaddress


def wg_allowed_ips(excluded):  
    allowed_networks = [ipaddress.ip_network('0.0.0.0/0')]    

    for exclude_network in excluded:
        curr_network = ipaddress.ip_network(exclude_network)
        new_networks = []
        for existing_network in allowed_networks:
            if existing_network.overlaps(curr_network):
                new_networks.extend(existing_network.address_exclude(curr_network))
            else:
                new_networks.append(existing_network)
        allowed_networks = new_networks    

    return ','.join(str(x) for x in sorted(allowed_networks))



def validate_network(test_network):
    try:
        ip = ipaddress.ip_network(test_network)
        return test_network
    except ValueError:
        raise argparse.ArgumentError(f"{test_network} does not appear to be a valid network.")
        sys.exit(1)


def main():
    parser = argparse.ArgumentParser(description='Generate allowed ips for wireguard config.')
    parser.add_argument('-e', '--exclude', type=validate_network, action='append',
                        help='ips or networks to exclude from wireguard.', required=True)

    args = parser.parse_args()
    print(wg_allowed_ips(args.exclude))


if __name__ =="__main__":
    main()