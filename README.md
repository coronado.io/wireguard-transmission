# [registry.gitlab.com/joshuajcoronado/wireguard-transmission](https://gitlab.com/joshuajcoronado/wireguard-transmission)

## About
This container attempts to pass your torrent traffic through a wireguard vpn provider to ensure a secure private connection. It relies on the work done by [linuxserver](https://www.linuxserver.io/) for their [wireguard](https://github.com/linuxserver/docker-wireguard) and [transmission](https://github.com/linuxserver/docker-transmission) containers. It takes inspiration from [binhex's](https://github.com/binhex) [arch-delugevpn](https://hub.docker.com/r/binhex/arch-delugevpn/).

[WireGuard](https://www.wireguard.com/) is an extremely simple yet fast and modern VPN that utilizes state-of-the-art cryptography. It aims to be faster, simpler, leaner, and more useful than IPsec, while avoiding the massive headache. It intends to be considerably more performant than OpenVPN. WireGuard is designed as a general purpose VPN for running on embedded interfaces and super computers alike, fit for many different circumstances. Initially released for the Linux kernel, it is now cross-platform (Windows, macOS, BSD, iOS, Android) and widely deployable. It is currently under heavy development, but already it might be regarded as the most secure, easiest to use, and simplest VPN solution in the industry.

[![wireguard](https://www.wireguard.com/img/wireguard.svg)](https://www.wireguard.com/)

[Transmission](https://www.transmissionbt.com/) is designed for easy, powerful use. Transmission has the features you want from a BitTorrent client: encryption, a web interface, peer exchange, magnet links, DHT, µTP, UPnP and NAT-PMP port forwarding, webseed support, watch directories, tracker editing, global and per-torrent speed limits, and more.

[![transmission](https://raw.githubusercontent.com/linuxserver/docker-templates/master/linuxserver.io/img/transmission.png)](https://www.transmissionbt.com/)

## Usage

Here are some example snippets to help you get started creating a container.

### docker

```
docker create \
  --name=wireguard \
  --cap-add=NET_ADMIN \
  --cap-add=SYS_MODULE \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/London \
  -e TRANSMISSION_WEB_HOME=/combustion-release/ `#optional` \
  -e USER=username `#optional` \
  -e PASS=password `#optional` \
  -p 51820:51820/udp \
  -p 9091:9091 \
  -p 51413:51413 \
  -p 51413:51413/udp \
  -v /path/to/appdata/config:/config \
  -v /lib/modules:/lib/modules \
  -v <path to downloads>:/downloads \
  -v <path to watch folder>:/watch \
  --sysctl="net.ipv4.conf.all.src_valid_mark=1" \
  --restart unless-stopped \
  linuxserver/wireguard
```

## Parameters

Container images are configured using parameters passed at runtime (such as those above). These parameters are separated by a colon and indicate `<external>:<internal>` respectively. For example, `-p 8080:80` would expose port `80` from inside the container to be accessible from the host's IP on port `8080` outside the container.

| Parameter | Function |
| :----: | --- |
| `-p 51820/udp` | wireguard port |
| `-p 9091` | WebUI |
| `-p 51413` | Torrent Port TCP |
| `-p 51413/udp` | Torrent Port UDP |
| `-e PUID=1000` | for UserID - see below for explanation |
| `-e PGID=1000` | for GroupID - see below for explanation |
| `-e TZ=Europe/London` | Specify a timezone to use EG Europe/London |
| `-e TRANSMISSION_WEB_HOME=/combustion-release/` | Specify an alternative UI options are `/combustion-release/`, `/transmission-web-control/`, and `/kettu/` . |
| `-e USER=username` | Specify an optional username for the interface |
| `-e PASS=password` | Specify an optional password for the interface |
| `-v /config` | Contains all relevant configuration files. |
| `-v /lib/modules` | Maps host's modules folder. |
| `-v /downloads` | Local path for downloads. |
| `-v /watch` | Watch folder for torrent files. |
| `--sysctl=` | Required for client mode. |

## Environment variables from files (Docker secrets)

You can set any environment variable from a file by using a special prepend `FILE__`. 

As an example:

```
-e FILE__PASSWORD=/run/secrets/mysecretpassword
```

Will set the environment variable `PASSWORD` based on the contents of the `/run/secrets/mysecretpassword` file.

## User / Group Identifiers

When using volumes (`-v` flags) permissions issues can arise between the host OS and the container, we avoid this issue by allowing you to specify the user `PUID` and group `PGID`.

Ensure any volume directories on the host are owned by the same user you specify and any permissions issues will vanish like magic.

In this instance `PUID=1000` and `PGID=1000`, to find yours use `id user` as below:

```
  $ id username
    uid=1000(dockeruser) gid=1000(dockergroup) groups=1000(dockergroup)
```


&nbsp;
## Wireguard - Application Setup

This image is designed for Ubuntu and Debian based systems only. During container start, it will download the necessary kernel headers and build the kernel module (until kernel 5.6, which has the module built-in, goes mainstream).

If you're on a debian/ubuntu based host with a custom or downstream distro provided kernel (ie. Pop!_OS), the container won't be able to install the kernel headers from the regular ubuntu and debian repos. In those cases, you can try installing the headers on the host via `sudo apt install linux-headers-$(uname -r)` (if distro version) and then add a volume mapping for `/usr/src:/usr/src`, or if custom built, map the location of the existing headers to allow the container to use host installed headers to build the kernel module (tested successful on Pop!_OS, ymmv).

With regards to arm32/64 devices, Raspberry Pi 2-4 running the [official ubuntu images](https://ubuntu.com/download/raspberry-pi) or Raspbian Buster are supported out of the box. For all other devices and OSes, you can try installing the kernel headers on the host, and mapping `/usr/src:/usr/src` and it may just work (no guarantees).

## Modifying your wireguard config

Usually, when submitting a VPN wireguard config, they'll have allowed ips set to `0.0.0.0/0` which will pass all traffic over the VPN interface. Unfortunately, that includes your transmission UI. So you'll need to generate the `AllowedIPs` section. To do this, you can use the attached script to do this. Notice you cannot simply use all public interfaces, as it will prevent you from initially connecting to your endpoint. The easiest way is
to run `./generate_allowed_ips.py` with your local network and wireguard endpoint.

```
>> ./generate_allowed_ips.py -e 192.168.1.0/24 -e 1.1.1.1
0.0.0.0/8,1.0.0.0/16,1.1.0.0/24,1.1.1.0/32,1.1.1.2/31,1.1.1.4/30,1.1.1.8/29,1.1.1.16/28,1.1.1.32/27,1.1.1.64/26,1.1.1.128/25,1.1.2.0/23,1.1.4.0/22,1.1.8.0/21,1.1.16.0/20,1.1.32.0/19,1.1.64.0/18,1.1.128.0/17,1.2.0.0/15,1.4.0.0/14,1.8.0.0/13,1.16.0.0/12,1.32.0.0/11,1.64.0.0/10,1.128.0.0/9,2.0.0.0/7,4.0.0.0/6,8.0.0.0/5,16.0.0.0/4,32.0.0.0/3,64.0.0.0/2,128.0.0.0/2,192.0.0.0/9,192.128.0.0/11,192.160.0.0/13,192.168.0.0/24,192.168.2.0/23,192.168.4.0/22,192.168.8.0/21,192.168.16.0/20,192.168.32.0/19,192.168.64.0/18,192.168.128.0/17,192.169.0.0/16,192.170.0.0/15,192.172.0.0/14,192.176.0.0/12,192.192.0.0/10,193.0.0.0/8,194.0.0.0/7,196.0.0.0/6,200.0.0.0/5,208.0.0.0/4,224.0.0.0/3
```
and you put that in your mullvad script
```
AllowedIPs = 0.0.0.0/8,1.0.0.0/16,1.1.0.0/24,1.1.1.0/32,1.1.1.2/31,1.1.1.4/30,1.1.1.8/29,1.1.1.16/28,1.1.1.32/27,1.1.1.64/26,1.1.1.128/25,1.1.2.0/23,1.1.4.0/22,1.1.8.0/21,1.1.16.0/20,1.1.32.0/19,1.1.64.0/18,1.1.128.0/17,1.2.0.0/15,1.4.0.0/14,1.8.0.0/13,1.16.0.0/12,1.32.0.0/11,1.64.0.0/10,1.128.0.0/9,2.0.0.0/7,4.0.0.0/6,8.0.0.0/5,16.0.0.0/4,32.0.0.0/3,64.0.0.0/2,128.0.0.0/2,192.0.0.0/9,192.128.0.0/11,192.160.0.0/13,192.168.0.0/24,192.168.2.0/23,192.168.4.0/22,192.168.8.0/21,192.168.16.0/20,192.168.32.0/19,192.168.64.0/18,192.168.128.0/17,192.169.0.0/16,192.170.0.0/15,192.172.0.0/14,192.176.0.0/12,192.192.0.0/10,193.0.0.0/8,194.0.0.0/7,196.0.0.0/6,200.0.0.0/5,208.0.0.0/4,224.0.0.0/3
```

## Wireguard - Client Mode
Drop your client conf into the config folder as `/config/wg0.conf` and start the container.

## Transmission - Application setup
Webui is on port 9091, the settings.json file in /config has extra settings not available in the webui. Stop the container before editing it or any changes won't be saved.

For users pulling an update and unable to access the webui setting you may need to set "rpc-host-whitelist-enabled": false, in /config/settings.json`

If you choose to use transmission-web-control as your default UI, just note that the origional Web UI will not be available to you despite the button being present. 

## Transmission - Securing the webui with a username/password.

Use the `USER` and `PASS` variables in docker run/create/compose to set authentication. Do not manually edit the `settings.json` to input user/pass, otherwise transmission cannot be stopped cleanly by the s6 supervisor.


